 

// Esfera.cpp: implementation of the Esfera class.
// MATRICULA -> 52074
// NOMBRE -> RODRIGO
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro.x+=velocidad.x*t;
centro.y+=velocidad.y*t;

radio-=0.05*t;
if(radio<0.05)
{
	radio=0.5;
}

//x1+=velocidad.x*t;
//x2+=velocidad.x*t;
//y1+=velocidad.y*t;
//y2+=velocidad.y*t;
}


